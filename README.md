Role Name
=========

install latest GitLab server

Requirements
------------

setup for redhat or debian

Role Variables
--------------

vars:
     gitlab_ce_version: "gitlab-ce-16.0.0-ce.0.el9.x86_64"

Dependencies
------------

setup for redhat or debian

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

 - name: Build the GitLab server
  hosts: gitlab_main
  become: true
  become_user: root

  pre_tasks:
    - name: Running the limit gitlab_main
      debug:
         msg: 'we are only running the gitlab main'

  vars:
     gitlab_ce_version: "gitlab-ce-16.0.0-ce.0.el9.x86_64"
     #gitlab_ce_version: "gitlab-ce-13.11.0-ce.0.el7.x86_64"

  roles:

     - role: skc_install_gitlab_server
       ceversion: "{{ gitlab_ce_version }}"
 
 
License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
